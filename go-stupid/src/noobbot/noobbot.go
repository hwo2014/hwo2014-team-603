package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
)

// just interfaces to other modules

type Msg interface {
}

type Envelope interface {
	GetTick() int
}

type Bot interface {
	SetTick(int)
	Join(BotId)
	GameInit(GameInit)
	GameStart(GameStart)
	GameEnd(GameEnd)
	YourCar(CarId)
	CarPositions(CarPositions) Msg
	TurboAvailable(Boost)
	Crash(Crash)
	Error(Error)
}

func main() {
	if len(os.Args) < 5 {
		log.Fatalf("usage: %s <host> <port> <botname> <botkey> [(c|j) <track> <pass> <count>]",
			os.Args[0])
	}

	host := os.Args[1]
	port, err := strconv.Atoi(os.Args[2])
	if err != nil {
		log.Fatalf("port is not numeric: %s", os.Args[2])
	}

	name := os.Args[3]
	key := os.Args[4]

	log.Printf("[init] %s:%d (%s:%s)", host, port, name, key)

	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		log.Fatalf("[connect] could not connect to server: %s", err)
	} else {
		log.Println("[connect] connection successfull")
	}

	bot := &Noob{}

	bot.StrategySelecter = CreateStrategySelecter(bot)

	if len(os.Args) >= 9 {
		track := os.Args[6]
		pass := os.Args[7]
		count, err := strconv.Atoi(os.Args[8])
		if err != nil {
			log.Fatalf("car count is not numeric: %s", os.Args[7])
		}

		if os.Args[5] == "c" {
			msgSend(conn, Race{BotId{name, key}, track, pass, count}, 0)
		} else {
			msgSend(conn, Join{BotId{name, key}, track, pass, count}, 0)
		}
	} else {
		msgSend(conn, BotId{name, key}, 0)
	}

	msgLoop(conn, bot)
}

func msgLoop(conn net.Conn, bot Bot) {
	reader := bufio.NewReader(conn)
	for {
		line, err := reader.ReadBytes('\n')
		if err != nil {
			log.Fatalf("[loop] err while reading line: %s", err)
		}

		log.Printf("[loop] >>>> %s", line)

		envelope := msgParse(line)
		reply := msgDispatch(envelope, bot)

		if reply == nil {
			log.Println("[loop] no response needed")
		} else {
			msgSend(conn, reply, envelope.GetTick())
		}
	}
}

func msgSend(conn net.Conn, msg Msg, tick int) {
	var packet Envelope
	switch m := msg.(type) {
	case Ping:
		packet = EnvelopePing{EnvelopeHead{"ping", tick}, m}
	case BotId:
		packet = EnvelopeJoin{EnvelopeHead{"join", tick}, m}
	case Join:
		packet = EnvelopeJoinRace{EnvelopeHead{"joinRace", tick}, m}
	case Race:
		packet = EnvelopeCreateRace{EnvelopeHead{"createRace", tick}, m}
	case Throttle:
		packet = EnvelopeThrottle{EnvelopeHead{"throttle", tick}, m}
	case Switch:
		packet = EnvelopeSwitch{EnvelopeHead{"switchLane", tick}, m}
	case Turbo:
		packet = EnvelopeTurbo{EnvelopeHead{"turbo", tick}, m}

	default:
		log.Fatalf("[send] unknown msg type: %#v", msg)
	}

	line, err := json.Marshal(packet)
	if err != nil {
		log.Fatalf("[send] could not marshall json: ", err)
	}

	_, err = conn.Write(append(line, '\n'))
	if err != nil {
		log.Fatalf("[send] could not write msg: %s", err)
	} else {
		log.Printf("[send] <<<< %s", line)
	}
}

func msgParse(line []byte) Envelope {
	head := EnvelopeHead{}
	json.Unmarshal(line, &head)

	var envelope Envelope

	switch head.Type {
	case "join":
		envelope = &EnvelopeJoin{}
	case "gameInit":
		envelope = &EnvelopeGameInit{}
	case "carPositions":
		envelope = &EnvelopeCarPositions{}
	case "yourCar":
		envelope = &EnvelopeYourCar{}
	case "gameStart":
		envelope = &EnvelopeGameStart{}
	case "error":
		envelope = &EnvelopeError{}
	case "crash":
		envelope = &EnvelopeCrash{}
	case "turboAvailable":
		envelope = &EnvelopeBoost{}
	default:
		log.Printf("[parse] unknown msg type: %s", head.Type)
		return head
	}

	err := json.Unmarshal(line, envelope)
	if err != nil {
		log.Printf("[parse] could not parse msg: %s", err)
		return nil
	}

	return envelope
}

func msgDispatch(envelope Envelope, bot Bot) Msg {
	switch e := envelope.(type) {
	case *EnvelopeJoin:
		bot.Join(e.Data)
		return nil
	case *EnvelopeCarPositions:
		bot.SetTick(e.Tick)
		msg := bot.CarPositions(e.Data)
		return msg
	case *EnvelopeBoost:
		bot.TurboAvailable(e.Data)
	case *EnvelopeGameInit:
		bot.GameInit(e.Data)
	case *EnvelopeGameStart:
		bot.GameStart(e.Data)
	case *EnvelopeYourCar:
		bot.YourCar(e.Data)
		return nil
	case *EnvelopeCrash:
		bot.Crash(e.Data)
	case *EnvelopeError:
		bot.Error(e.Data)
	default:
		log.Printf("[loop] undeclared action: %#v", envelope)
	}

	return Ping{}
}
