package main

// import (
//   "log"
// )

type StrategyTurboOnLine struct {
	Bot                  *Noob
	BestLane             int
	LastSwitchPieceIndex int
}

func (strategy StrategyTurboOnLine) Name() string {
	return "turbo_on_line"
}

func (strategy *StrategyTurboOnLine) Check() bool {
	bot := strategy.Bot
	_, distance := bot.GetNextTurnInfo(bot.Pos.Index, bot.Pos.Distance)

	if strategy.Bot.Boost.TurboDurationTicks == 0 {
		return false
	}

	if distance < 200 {
		return false
	}

	return true
}

func (strategy StrategyTurboOnLine) GetActions() (float64, string, bool) {
	strategy.Bot.Boost.TurboDurationTicks = 0
	return 0, "", true
}
