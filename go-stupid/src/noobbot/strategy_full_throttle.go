package main

func (strategy StrategyFullThrottle) Name() string {
	return "full_throttle"
}

type StrategyFullThrottle struct {
	Throttle float64
	Bot      *Noob
}

func (strategy StrategyFullThrottle) Check() bool {
	return true
}

func (strategy StrategyFullThrottle) GetActions() (float64, string, bool) {
	return strategy.Throttle, "", false
}
