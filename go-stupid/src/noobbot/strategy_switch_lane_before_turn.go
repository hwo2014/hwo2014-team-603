package main

type StrategySwitchLaneBeforeTurn struct {
	Bot      *Noob
	BestLane int
}

func (strategy StrategySwitchLaneBeforeTurn) Name() string {
	return "switch_lane_before_turn"
}

func (strategy *StrategySwitchLaneBeforeTurn) Check() bool {
	bot := strategy.Bot

	if !bot.isNextSwitchOnNextTick() {
		return false
	}

	switchPieceIndex, _ := bot.GetNextSwitchInfo(bot.Pos.Index, bot.Pos.Distance)
	switchPieceIndex, _ = bot.GetNextSwitchInfo(switchPieceIndex, 0)
	tempIndex := bot.Pos.Index

	angleSum := 0.0

	for tempIndex, piece := bot.GetNextPiece(tempIndex); tempIndex != switchPieceIndex; tempIndex, piece = bot.GetNextPiece(tempIndex) {

		angleSum += piece.Angle
	}

	bestLane := 0
	if angleSum == 0 {
		return false
	} else if angleSum > 0 {
		maxDistance := 0.0
		for _, lane := range bot.Track.Lanes {
			if lane.DistanceFromCenter > maxDistance {
				maxDistance = lane.DistanceFromCenter
				bestLane = lane.Index
			}
		}
	} else {
		minDistance := 0.0
		for _, lane := range bot.Track.Lanes {
			if lane.DistanceFromCenter < minDistance {
				minDistance = lane.DistanceFromCenter
				bestLane = lane.Index
			}
		}
	}

	if bot.Pos.Lane.End == bestLane {
		return false
	}

	strategy.BestLane = bestLane

	return true
}

func (strategy StrategySwitchLaneBeforeTurn) GetActions() (float64, string, bool) {
	return 0, strategy.Bot.GetSwitchForLane(strategy.BestLane), false
}
