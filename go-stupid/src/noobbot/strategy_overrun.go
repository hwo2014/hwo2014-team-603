package main

import (
	// "log"
	"math"
)

type StrategyOverrun struct {
	Bot             *Noob
	maximalVelocity float64
	BestLane        int
}

func (strategy StrategyOverrun) Name() string {
	return "overrun"
}

func (strategy *StrategyOverrun) Check() bool {
	bot := strategy.Bot

	if !bot.isNextSwitchOnNextTick() {
		return false
	}

	opponentDistance, opponent := bot.getClosestOpponentBetweenTwoSwitches(
		bot.Pos.Lane.End)

	if opponent == nil {
		return false
	}

	bestLaneFound, bestLane := strategy.getBestLane(opponentDistance)
	if !bestLaneFound {
		return false
	}

	strategy.BestLane = bestLane
	return true
}

func (strategy StrategyOverrun) GetActions() (float64, string, bool) {
	return 0, strategy.Bot.GetSwitchForLane(strategy.BestLane), false
}

func (strategy StrategyOverrun) getBestLane(opponentDistance float64) (bool, int) {
	bot := strategy.Bot

	prevLaneOpponentDistance := 0.0
	nextLaneOpponentDistance := 0.0
	var prevLaneOpponent *CarPosition
	var nextLaneOpponent *CarPosition

	prevLane := bot.Pos.Lane.End - 1
	if prevLane >= 0 {
		prevLaneOpponentDistance, prevLaneOpponent =
			bot.getClosestOpponentBetweenTwoSwitches(prevLane)

		if prevLaneOpponent == nil {
			return true, prevLane
		}
	}

	nextLane := bot.Pos.Lane.End + 1
	if nextLane <= len(bot.Track.Lanes)-1 {
		nextLaneOpponentDistance, nextLaneOpponent =
			bot.getClosestOpponentBetweenTwoSwitches(nextLane)

		if nextLaneOpponent == nil {
			return true, nextLane
		}
	}

	if opponentDistance > math.Min(nextLaneOpponentDistance,
		prevLaneOpponentDistance) {

		if nextLaneOpponentDistance < prevLaneOpponentDistance {
			return true, nextLane
		} else {
			return true, prevLane
		}
	}

	return false, 0
}

func (bot Noob) getClosestOpponent(lane int) (float64, *CarPosition) {
	minDist := 0.0
	minOpponentIndex := 0
	for opponentIndex, opponent := range bot.Opponents {
		dist := bot.GetDistanceTo(opponent.Position.Index,
			opponent.Position.Distance)

		if lane == opponent.Position.Lane.End && (dist < minDist ||
			minDist == 0) {

			minDist = dist
			minOpponentIndex = opponentIndex
		}
	}

	if minDist == 0 {
		return 0, nil
	}

	return minDist, &bot.Opponents[minOpponentIndex]
}

func (bot Noob) getClosestOpponentBetweenTwoSwitches(lane int) (float64,
	*CarPosition) {

	opponentDistance, opponent := bot.getClosestOpponent(lane)
	if opponent == nil {
		return 0, nil
	}

	switchIndex, switchDistance := bot.GetNextSwitchInfo(bot.Pos.Index,
		bot.Pos.Distance)

	_, nextSwitchAfterDistance := bot.GetNextSwitchInfo(
		switchIndex, 0)

	if opponentDistance < switchDistance {
		return 0, nil
	}

	if switchDistance+nextSwitchAfterDistance < opponentDistance {
		return 0, nil
	}

	return opponentDistance, opponent
}
