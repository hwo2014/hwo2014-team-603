package main

type EnvelopeHead struct {
	Type string `json:"msgType"`
	Tick int    `json:"gameTick"`
}

func (eh EnvelopeHead) GetTick() int {
	return eh.Tick
}

type EnvelopeJoin struct {
	EnvelopeHead
	Data BotId `json:"data"`
}

type EnvelopeCreateRace struct {
	EnvelopeHead
	Data Race `json:"data"`
}

type EnvelopeJoinRace struct {
	EnvelopeHead
	Data Join `json:"data"`
}

type EnvelopeGameInit struct {
	EnvelopeHead
	Data GameInit `json:"data"`
}

type EnvelopeCarPositions struct {
	EnvelopeHead
	Data CarPositions `json:"data"`
}

type EnvelopeYourCar struct {
	EnvelopeHead
	Data CarId `json:"data"`
}

type EnvelopeGameStart struct {
	EnvelopeHead
	Data GameStart `json:"data"`
}

type EnvelopeError struct {
	EnvelopeHead
	Data Error `json:"data"`
}

type EnvelopeCrash struct {
	EnvelopeHead
	Data Crash `json:"data"`
}

type EnvelopePing struct {
	EnvelopeHead
	Data struct{} `json:"data"`
}

type EnvelopeThrottle struct {
	EnvelopeHead
	Data Throttle `json:"data"`
}

type EnvelopeSwitch struct {
	EnvelopeHead
	Data Switch `json:"data"`
}

type EnvelopeTurbo struct {
	EnvelopeHead
	Data Turbo `json:"data"`
}

type EnvelopeBoost struct {
	EnvelopeHead
	Data Boost `json:"data"`
}

type Ping struct {
	// struct intentionaly left empty
}

type Error string

type Crash struct {
	// @TODO implement
}

type BotId struct {
	Name string `json:"name"`
	Key  string `json:"key"`
}

type GameInit struct {
	Race RaceInfo `json:"race"`
	// @TODO other fields
}

type RaceInfo struct {
	Track Track `json:"track"`
	Cars  []Car `json:"cars"`
	// @TODO other fields
}

type Track struct {
	Id     string  `json:"id"`
	Name   string  `json:"name"`
	Pieces []Piece `json:"pieces"`
	Lanes  []Lane  `json:"lanes"`
}

type Piece struct {
	Length float64 `json:"length"`
	Switch bool    `json:"switch"`
	Radius float64 `json:"radius"`
	Angle  float64 `json:"angle"`
}

type Lane struct {
	Index              int     `json:"index"`
	DistanceFromCenter float64 `json:"distanceFromCenter"`
}

type CarId struct {
	Name  string `json:"name"`
	Color string `json:"color"`
}

type Car struct {
	Id         CarId         `json:"id"`
	Dimensions CarDimensions `json:"dimensions"`
}

type CarDimensions struct {
	Length  float64 `json:"length"`
	Width   float64 `json:"width"`
	FlagPos float64 `json:"guideFlagPosition"`
}

type PiecePosition struct {
	Index    int          `json:"pieceIndex"`
	Distance float64      `json:"inPieceDistance"`
	Lane     LanePosition `json:"lane"`
}

type LanePosition struct {
	Start int `json:"startLaneIndex"`
	End   int `json:"endLaneIndex"`
}

type CarPositions []CarPosition

type CarPosition struct {
	Id       CarId         `json:"id"`
	Angle    float64       `json:"angle"`
	Position PiecePosition `json:"piecePosition"`
	Lap      int           `json:"lap"`
}

type GameStart struct {
	// @TODO implement
}

type GameEnd struct {
	// @TODO implement
}

type Race struct {
	Bot       BotId  `json:"botId"`
	TrackName string `json:"trackName"`
	Password  string `json:"password"`
	CarCount  int    `json:"carCount"`
}

type Join struct {
	Bot       BotId  `json:"botId"`
	TrackName string `json:"trackName"`
	Password  string `json:"password"`
	CarCount  int    `json:"carCount"`
}

type Boost struct {
	TurboDurationMilliseconds float64 `json:"turboDurationMilliseconds"`
	TurboDurationTicks        int     `json:"turboDurationTicks"`
	TurboFactor               float64 `json:"turboFactor"`
}

type Throttle float64
type Switch string
type Turbo string

var SwitchRight = Switch("Right")
var SwitchLeft = Switch("Left")
