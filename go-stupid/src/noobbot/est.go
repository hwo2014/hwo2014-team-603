package main

import (
	"log"
)

type Line struct {
	A float64
	B float64
}

func (l Line) Eval(x float64) float64 {
	return l.A*x + l.B
}

type AvgLineEstimator struct {
	Lines []Line
	Num   int
}

func (est *AvgLineEstimator) Estimate(x1, x2, y1, y2 float64) {
	est.Lines = append(est.Lines, EstimateLine(x1, x2, y1, y2))
	est.Num += 1
}

func (est *AvgLineEstimator) Eval(x float64, depth int) float64 {
	n := len(est.Lines)
	estimator := Line{}
	count := 0.0
	for i := n - 1; i >= n-depth && i >= 0; i-- {
		estimator.A += est.Lines[i].A
		estimator.B += est.Lines[i].B
		count += 1
	}

	estimator.A /= count
	estimator.B /= count

	log.Println(estimator)

	return estimator.Eval(x)
}

func EstimateLine(x1, x2, y1, y2 float64) Line {
	if x1-x2 != 0 {
		a := (y2 - y1) / (x2 - x1)
		b := (x2*y1 - x1*y2) / (x2 - x1)

		return Line{a, b}
	} else {
		return Line{0, 0}
	}
}
