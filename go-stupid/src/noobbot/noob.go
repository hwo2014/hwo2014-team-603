package main

import (
	"log"
	"math"
	"math/rand"
	"time"
)

var _ = math.E

type Noob struct {
	Tick              int
	Car               Car
	Track             Track
	Accel             float64
	Velocity          float64
	Angle             float64
	Pos               PiecePosition
	Piece             Piece
	Throttle          float64
	Boost             Boost
	Turbo             bool
	Switch            string
	Lab               interface{}
	History           map[int]HistoryElement
	AccelLine         Line
	TurnRadius        float64
	TurnEnterAngleAcc float64
	AngleVelocity     float64
	AngleAccel        float64
	StrategySelecter  *StrategySelecter
	Opponents         []CarPosition
}

type HistoryElement struct {
	Throttle      float64
	Pos           PiecePosition
	Velocity      float64
	Accel         float64
	AccelErr      float64
	Angle         float64
	Piece         Piece
	AngleVelocity float64
	AngleAccel    float64
}

func (b *Noob) SetTick(tick int) {
	b.Tick = tick
}

func (b *Noob) Join(bi BotId) {
	log.Println("[noob] joined")
}

func (b *Noob) GameInit(gi GameInit) {
	log.Println("[noob] game init")
	// @todo We need to sort lanes here from minimal distance to maximal and
	// then map it in strategies
	b.Track = gi.Race.Track

	for _, car := range gi.Race.Cars {
		log.Println(car)
		if car.Id == b.Car.Id {
			b.Car = car
			break
		}
	}

	b.History = make(map[int]HistoryElement)

	log.Printf("[noob] my car info: %#v", b.Car)
}

func (b *Noob) GameStart(gs GameStart) {
	log.Println("[noob] game start")
}

func (b *Noob) GameEnd(ge GameEnd) {
	log.Println("[noob] game end")
}

func (b *Noob) YourCar(c CarId) {
	log.Printf("[noob] my car is %s", c.Color)
	b.Car.Id = c
}

func (b *Noob) TurboAvailable(boost Boost) {
	log.Println(Scolorf("green", "[noob] TURBO AVAILABLE: %d", b.Pos.Index))
	b.Boost = boost
}

func (b *Noob) CarPositions(cp CarPositions) Msg {
	// log.Println((Scolorf("magenta", "[noob] tick %d", b.Tick)))
	if b.Tick == 0 {
		return nil
	}

	strategy := b.StrategySelecter.Select()
	if strategy != nil {
		b.Throttle, b.Switch, b.Turbo = strategy.GetActions()
	}

	rand.Seed(time.Now().Unix())
	b.Throttle = rand.Float64()*0.2 + 0.2

	b.findAngleAndPos(cp)
	b.calcVelocity()
	b.calcAccel()
	b.calcMyPiece()

	//prevHist := b.History[b.Tick-1]
	//prevPiece := prevHist.Piece
	//angleAcc := prevHist.AngleVelocity - b.AngleVelocity
	//if b.Piece.Angle != prevPiece.Angle && b.Piece.Radius != prevPiece.Radius {
	//    b.TurnEnterAngleAcc = angleAcc
	//    log.Println(Scolorf("red", "[noob] enter_velocity: %f", b.TurnEnterAngleAcc))
	//}

	//log.Println(Scolorf("red", "[noob] in_piece_angle_acc: %f (%f)",
	//    angleAcc-b.TurnEnterAngleAcc, angleAcc))

	log.Println(Scolorf("yellow", "[noob] strategy: %s", strategy.Name()))
	log.Println(Scolorf("yellow", "[noob] piece: %d", b.Pos.Index))
	log.Println(Scolorf("green", "[noob] velocity: %f", b.Velocity))
	log.Println(Scolorf("green", "[noob] angle: %f", b.Angle))
	log.Println(Scolorf("green", "[noob] angle_vel: %f", b.AngleVelocity))
	log.Println(Scolorf("green", "[noob] angle_acc: %f", b.AngleAccel))
	log.Println(Scolorf("red", "[noob] throttle: %f", b.Throttle))
	// log.Println(Scolorf("green", "[noob] turn_angle: %f", b.Piece.Angle))

	//b.calcResultForce()
	b.keepHistoryGoing()

	// can be calculated for any velocity/throttle
	// nextAccel := b.calcNextAcceleration(b.Velocity, b.Throttle)
	// log.Println(Scolorf("red", "[noob] est. next accel: %f",
	// nextAccel))

	if b.Turbo == true {
		b.Turbo = false
		return Turbo("")
	}

	if b.Switch != "" {
		laneSwitch := b.Switch
		b.Switch = ""
		return Switch(laneSwitch)
	}

	if b.Throttle >= 0 {
		return Throttle(b.Throttle)
	}

	return Ping{}
}

func (b *Noob) Crash(c Crash) {
	log.Println("[noob] CRASH: ", c)
	// log.Fatalf("[noob] CRASH: ", c)
}

func (b *Noob) Error(e Error) {
	log.Println("[noob] ERROR: ", e)
}

func (b *Noob) calcMyPiece() {
	b.Piece = b.Track.Pieces[b.Pos.Index]
	if b.Piece.Radius > 0 {
		laneDist := b.Track.Lanes[b.Pos.Lane.Start].DistanceFromCenter
		if b.Piece.Angle > 0 {
			b.TurnRadius = b.Piece.Radius - laneDist
		} else {
			b.TurnRadius = b.Piece.Radius + laneDist
		}
	} else {
		b.TurnRadius = 0
	}
}

func (b *Noob) calcResultForce() {
	carLength := b.Car.Dimensions.Length
	backLength := carLength - b.Car.Dimensions.FlagPos

	backMassCoeff := backLength / carLength

	// @FIXME сейчас используется приближение,
	// проекция скорости задницы машины на ось заноса может не быть равна
	// линейной скорости машины
	//backLinearVel := b.Velocity

	// @FIXME сейчас используется приближение,
	// расстояние от центра поворота до середины жопы машины больше, чем радиус
	// поворота
	//backRadius := b.Piece.Radius

	backDistance := backLength / 2

	rad := math.Pi / 180.0

	xa := b.TurnRadius
	xb := backDistance
	xg := b.Angle + 90
	xc := math.Sqrt(xa*xa + xb*xb - 2*xa*xb*math.Cos(xg*rad))

	centrifugalToBackAngle := 90 - math.Acos((xb*xb+xc*xc-xa*xa)/(2*xb*xc))/rad
	backRadius := xc

	backLinearVel := b.Velocity + (b.AngleVelocity * backDistance * rad)

	f := b.calcNextAcceleration

	frictionCoeff := -f(b.Velocity, 0)

	// текущий или предыдущий тротл?
	tractionForce := f(b.Velocity, b.Throttle) + frictionCoeff

	frictionForce := frictionCoeff /* multiply by mass */

	backCentrifugalForce := 0.0
	if b.TurnRadius > 0 {
		backCentrifugalForce = backMassCoeff * backLinearVel * backLinearVel / backRadius
	}

	projTractionForce := tractionForce * math.Sin(math.Abs(b.Angle*rad))
	//projFrictionForce := frictionForce * math.Sin(math.Abs(b.Angle*rad))

	projBackCentrifugalForce := backCentrifugalForce * math.Cos(centrifugalToBackAngle*rad)

	sideFrictionCoeff := 0.315
	sideFrictionForce := backMassCoeff * sideFrictionCoeff // * math.Sqrt2
	projSideFrictionForce := sideFrictionForce

	projReturnForce := 0.0
	projReturnForce += projTractionForce
	//projReturnForce -= math.Min(projTractionForce, projSideFrictionForce)

	//resultForce := 0.0
	//if math.Abs(projBackCentrifugalForce) > sideFrictionForce {
	//    resultForce = projBackCentrifugalForce - sideFrictionForce
	//} else {
	//    resultForce = 0
	//}

	resultForce := 0.0
	//resultForce -= math.Max(projReturnForce-projFrictionForce, 0)

	resultForce += projBackCentrifugalForce
	if math.Abs(resultForce) < sideFrictionForce {
		resultForce = 0
	} else {
		if resultForce <= 0 {
			resultForce += projSideFrictionForce
		} else {
			resultForce -= projSideFrictionForce
		}
	}

	resultForce -= projReturnForce

	//if b.Piece.Angle < 0 {
	//    resultForce *= -1
	//}

	//resultForce *= 4 // multiply by mass

	angleLinearAccel := resultForce / backMassCoeff
	angleLinearVel := b.AngleVelocity * rad * backDistance
	angleAccel := (angleLinearVel + angleLinearAccel) / backDistance / rad

	log.Println(Scolorf("yellow", "[noob] carLength: %f",
		carLength))
	log.Println(Scolorf("yellow", "[noob] backLength: %f",
		backLength))
	log.Println(Scolorf("yellow", "[noob] backMassCoeff: %f",
		backMassCoeff))
	log.Println(Scolorf("yellow", "[noob] backLinearVel: %f",
		backLinearVel))
	log.Println(Scolorf("yellow", "[noob] backRadius: %f",
		backRadius))
	log.Println(Scolorf("yellow", "[noob] frictionCoeff: %f",
		frictionCoeff))
	log.Println(Scolorf("yellow", "[noob] frictionForce: %f",
		frictionForce))
	log.Println(Scolorf("yellow", "[noob] sideFrictionCoeff: %f",
		sideFrictionCoeff))
	log.Println(Scolorf("yellow", "[noob] sideFrictionForce: %f",
		sideFrictionForce))
	log.Println(Scolorf("yellow", "[noob] tractionForce: %f",
		tractionForce))
	log.Println(Scolorf("yellow", "[noob] projTractionForce: %f",
		projTractionForce))
	log.Println(Scolorf("yellow", "[noob] backCentrifugalForce: %f",
		backCentrifugalForce))
	log.Println(Scolorf("yellow", "[noob] centrifugalToBackAngle: %f",
		centrifugalToBackAngle))
	log.Println(Scolorf("yellow", "[noob] projBackCentrifugalForce: %f",
		projBackCentrifugalForce))
	log.Println(Scolorf("yellow", "[noob] projReturnForce: %f",
		projReturnForce))
	log.Println(Scolorf("yellow", "[noob] resultForce: %f",
		resultForce))
	log.Println(Scolorf("yellow", "[noob] angleAccel: %f",
		angleAccel))
}

func (b *Noob) estimateAccelFunction() (Msg, bool) {
	if b.Tick >= 1 {
		if b.Tick < 3 {
			// 0.5 is magic
			return Throttle(0.5), true
		}

		if b.Tick == 3 {
			b.AccelLine = EstimateLine(b.History[2].Velocity,
				b.History[1].Velocity, b.Accel, b.History[2].Accel)
		}
	}

	return Ping{}, false
}

func (b *Noob) calcNextAcceleration(velocity float64, throttle float64) float64 {
	// magic
	tCoeff := 1.0 / 5.0
	return b.AccelLine.Eval(velocity) + tCoeff*(throttle-0.5)
}

func (b *Noob) chooseAction() Msg {
	if msg, ok := b.estimateAccelFunction(); ok {
		return msg
	}

	return Throttle(0.6)
}

func (b *Noob) calcVelocity() {
	if b.Tick <= 1 {
		b.Velocity = 0.0
	} else {
		//prevVelocity := b.History[b.Tick-1].Velocity
		prevPos := b.History[b.Tick-1].Pos

		if prevPos.Index == b.Pos.Index {
			b.Velocity = b.Pos.Distance - prevPos.Distance
		} else {
			// @FIXME calc arc length, now velocity is not calculated in between arcs
			if b.Track.Pieces[prevPos.Index].Length > 0 {
				b.Velocity = b.Track.Pieces[prevPos.Index].Length -
					prevPos.Distance + b.Pos.Distance
			}
		}
	}
}

func (b *Noob) findAngleAndPos(cp CarPositions) {
	for _, p := range cp {
		if p.Id == b.Car.Id {
			b.Pos = p.Position
			b.AngleAccel = (p.Angle - b.Angle) - b.AngleVelocity
			b.AngleVelocity = p.Angle - b.Angle
			b.Angle = p.Angle
		} else {
			b.Opponents = append(b.Opponents, p)
		}
	}
}

func (b *Noob) keepHistoryGoing() {
	b.History[b.Tick] = HistoryElement{
		Pos:           b.Pos,
		Throttle:      b.Throttle,
		Velocity:      b.Velocity,
		Accel:         b.Accel,
		Angle:         b.Angle,
		AngleVelocity: b.AngleVelocity,
		AngleAccel:    b.AngleAccel,
		Piece:         b.Piece,
	}
}

func (b *Noob) calcAccel() {
	if b.Tick <= 1 {
		b.Accel = 0
	} else {
		b.Accel = b.Velocity - b.History[b.Tick-1].Velocity
	}
}

//func (b *Noob) calcVelocityIn(ticks int, throttle float64) float64 {
//    velocity := b.Velocity
//    accel := b.Accel
//    for i := 0; i < ticks; i++ {
//        accel = b.AccelModel.CalcAccel(velocity+accel, throttle) -
//            b.AccelErrModel.CalcAccelError(velocity+accel)
//        velocity += accel
//    }

//    return velocity
//}
