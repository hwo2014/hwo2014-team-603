package main

import (
	"log"
)

type StrategyTurnAhead struct {
	Bot   *Noob
	Debug bool
}

func (strategy StrategyTurnAhead) Name() string {
	return "turn_ahead"
}

func (strategy StrategyTurnAhead) Check() bool {
	bot := strategy.Bot

	// не работает для последнего участка пути
	if bot.Pos.Index+1 > len(bot.Track.Pieces)-1 {
		if strategy.Debug {
			log.Println(Scolorf("red", "STRATEGY REJECTED: %s (1)", strategy.Name()))
		}

		return false
	}

	currentPiece := bot.Track.Pieces[bot.Pos.Index]
	nextPiece := bot.Track.Pieces[bot.Pos.Index+1]

	// работает только для прямых участков и следующего участка с радиусом
	if currentPiece.Length == 0 || nextPiece.Radius == 0 {
		if strategy.Debug {
			log.Println(Scolorf("red", "STRATEGY REJECTED: %s (2)", strategy.Name()))
		}

		return false
	}

	maximalTurnVelocity := bot.GetMaximalPreTurnVelocity(nextPiece.Radius,
		nextPiece.Angle, bot.Angle)

	nextTickPosition := bot.GetPositionAfterTicks(1, bot.Pos.Distance,
		bot.Velocity, 1)

	nextTickFinalVelocity := bot.GetVelocityAtPosition(currentPiece.Length-
		nextTickPosition, bot.Velocity, 0)

	finalVelocity := bot.GetVelocityAtPosition(currentPiece.Length-
		bot.Pos.Distance, bot.Velocity, 0)

	if strategy.Debug {
		log.Println(Scolorf("red", "STRATEGY TEST: %s %f = %f %f = %f (2)", strategy.Name(),
			currentPiece.Length-nextTickPosition, nextTickFinalVelocity, currentPiece.Length-
				bot.Pos.Distance, finalVelocity))
	}

	// можно безопасно тормозить со следующего тика, ждём следующий тик
	if nextTickFinalVelocity <= maximalTurnVelocity {
		if strategy.Debug {
			log.Println(Scolorf("red", "STRATEGY REJECTED: %s %f (3)",
				strategy.Name(), nextTickFinalVelocity))
		}

		return false
	}

	// алгоритмы подстраивался под скорость торможения, поэтому я получал здесь
	// ложное срабатывание;
	//
	// временно отключил, надеемся на то, что успеем затормозить на нужной
	// скорости
	//
	// finalVelocity := bot.GetVelocityAtPosition(currentPiece.Length, bot.Velocity,
	//   bot.Pos.Distance, 0)
	//
	// торможение ничего не даст, не применяем стратегию
	// if finalVelocity > maximalTurnVelocity {
	//   log.Println(Scolorf("red", "STRATEGY REJECTED (4): %s", strategy.Name()))
	//   return false
	// }

	return true
}

func (strategy StrategyTurnAhead) GetActions() (float64, string, bool) {
	return 0, "", false
}

// - просчитать финальную скорость торможения от следующей позиции;
// - если торможение от следуюей позиции будет давать скорость большую, чем
// нужно для входа в поворот, то просчитать торможение от текущей позиции
// - если торможение от текущей позиции будет давать скорость меньшую скорости в
// поворот, применить стратегию
