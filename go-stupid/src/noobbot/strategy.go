package main

import ()

// usage:
//
// 1. Implement interface Strategy
// 2. Add your own strategies at CreateStrategySelecter
// 3. Strategy checks and executes in the order of adding - earlier added - earlier checked and executed
// 3. Profit

type Strategy interface {
	Name() string // only for debug
	Check() bool
	GetActions() (float64, string, bool) // throttle, boost
}

type StrategySelecter struct {
	Strategies []Strategy
}

func (selecter StrategySelecter) Select() Strategy {
	for _, strategy := range selecter.Strategies {
		if strategy.Check() {
			return strategy
		}
	}

	return nil
}

func CreateStrategySelecter(bot *Noob) *StrategySelecter {
	selecter := new(StrategySelecter)

	// source := rand.NewSource(99)

	// set strategies
	simple := new(StrategyFullThrottle)
	simple.Bot = bot
	simple.Throttle = 0.5
	selecter.Strategies = append(selecter.Strategies, simple)

	return selecter
}
