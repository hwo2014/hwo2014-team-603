package main

type StrategyAccelLineEstimation struct {
	Bot             *Noob
	maximalVelocity float64
}

func (strategy StrategyAccelLineEstimation) Name() string {
	return "accel_line_estimation"
}

func (strategy StrategyAccelLineEstimation) Check() bool {
	bot := strategy.Bot

	if bot.Tick == 3 {
		bot.AccelLine = EstimateLine(bot.History[2].Velocity,
			bot.History[1].Velocity, bot.Accel, bot.History[2].Accel)
	}

	if bot.Tick >= 1 && bot.Tick < 3 {
		return true
	}

	return false
}

func (strategy StrategyAccelLineEstimation) GetActions() (float64, string, bool) {
	return 0.5, "", false
}
