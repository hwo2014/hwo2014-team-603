package main

import (
	"math"
)

func (bot Noob) GetMaximalVelocityOnTurn(radius float64,
	angle float64, botAngle float64) float64 {

	// if math.Abs(botAngle) > 45 {
	//   return 10
	// }

	speed := 1 + radius/25.0 + (1 - math.Abs(botAngle/1.5)/40.0) - 2 // hack it please...
	if speed < 3 {
		speed = 3
	}

	return speed
}

func (bot Noob) GetMaximalPreTurnVelocity(radius float64,
	angle float64, botAngle float64) float64 {

	if math.Abs(botAngle) > 45 {
		return 10
	}

	speed := 3 + radius/25.0 - math.Abs(angle+botAngle)/15 - 2 // hack it please...
	if speed < 3 {
		speed = 3
	}

	return speed
}

func (bot Noob) GetVelocityAtPosition(finalPosition float64,
	currentVelocity float64, throttle float64) float64 {

	for currentPosition := 0.0; currentPosition < finalPosition; {
		currentVelocity += bot.calcNextAcceleration(currentVelocity, throttle)
		currentPosition += currentVelocity

		if currentVelocity < 0.0005 {
			return 0
		}
	}

	return currentVelocity
}

func (bot Noob) GetPositionAfterTicks(ticks int, currentPosition float64,
	currentVelocity float64, throttle float64) float64 {

	for tick := 0; tick < ticks; tick += 1 {
		currentVelocity += bot.calcNextAcceleration(currentVelocity, throttle)
		currentPosition += currentVelocity
	}

	return currentPosition
}

func (bot Noob) GetVelocityAfterTicks(ticks int, currentVelocity float64,
	throttle float64) float64 {

	for tick := 0; tick < ticks; tick += 1 {
		currentVelocity += bot.calcNextAcceleration(currentVelocity, throttle)
	}

	return currentVelocity
}

func (bot Noob) GetNextTurnInfo(currenPieceIndex int,
	currenPiecePosition float64) (int, float64) {

	startIndex := currenPieceIndex + 1
	piecesMaxIndex := len(bot.Track.Pieces) - 1
	if startIndex > piecesMaxIndex {
		startIndex = 0
	}

	distance := 0.0
	pieceIndex := startIndex
	for ; pieceIndex != currenPieceIndex; pieceIndex += 1 {
		if pieceIndex > piecesMaxIndex {
			pieceIndex = 0
		}

		if bot.Track.Pieces[pieceIndex].Radius != 0 {
			break
		}

		distance += bot.GetPieceLength(pieceIndex)
	}

	return pieceIndex, distance
}

func (bot Noob) GetNextSwitchInfo(currenPieceIndex int,
	currenPiecePosition float64) (int, float64) {

	startIndex := currenPieceIndex + 1
	piecesMaxIndex := len(bot.Track.Pieces) - 1
	if startIndex > piecesMaxIndex {
		startIndex = 0
	}

	distance := 0.0
	pieceIndex := startIndex
	for ; pieceIndex != currenPieceIndex; pieceIndex += 1 {
		if pieceIndex > piecesMaxIndex {
			pieceIndex = 0
		}

		if bot.Track.Pieces[pieceIndex].Switch == true {
			break
		}

		distance += bot.GetPieceLength(pieceIndex)
	}

	return pieceIndex, distance
}

func (bot Noob) GetNextPiece(currenPieceIndex int) (int, Piece) {
	currenPieceIndex += 1
	piecesMaxIndex := len(bot.Track.Pieces) - 1
	if currenPieceIndex > piecesMaxIndex {
		currenPieceIndex = 0
	}

	return currenPieceIndex, bot.Track.Pieces[currenPieceIndex]
}

func (bot Noob) GetPieceLength(pieceIndex int) float64 {
	piece := bot.Track.Pieces[pieceIndex]
	if piece.Length > 0 {
		return piece.Length
	}

	return bot.GetTurnLength(piece.Radius, piece.Angle, bot.Pos.Lane.End)
}

func (bot Noob) GetTurnLength(radius float64, angle float64,
	laneIndex int) float64 {
	distanceFromCenter := 0.0

	for _, lane := range bot.Track.Lanes {
		if lane.Index == laneIndex {
			distanceFromCenter = lane.DistanceFromCenter
		}
	}

	radius += distanceFromCenter

	return angle / 180 * math.Pi * radius
}

func (bot Noob) GetAngleAcceleration(botAngleAcceleration float64,
	botAngle float64, botVelocity float64, turnRadius float64,
	throttle float64) float64 {

	return botAngleAcceleration + botAngle + botVelocity + turnRadius +
		throttle*(2^2/2)
}

func (bot Noob) GetDistanceTo(pieceIndex int, distance float64) float64 {
	for pieceIndex := bot.Pos.Index; pieceIndex != pieceIndex; pieceIndex += 1 {
		if pieceIndex > len(bot.Track.Pieces)-1 {
			pieceIndex = 0
		}

		distance += bot.GetPieceLength(pieceIndex)
	}

	return distance - bot.Pos.Distance
}

func (bot Noob) isNextSwitchOnNextTick() bool {
	_, nextPiece := bot.GetNextPiece(bot.Pos.Index)
	if nextPiece.Switch == false {
		return false
	}

	distanceToNextPiece := bot.GetPieceLength(bot.Pos.Index) - bot.Pos.Distance
	if distanceToNextPiece > bot.Velocity+2 { // ?
		return false
	}

	return true
}

func (bot Noob) GetSwitchForLane(lane int) string {
	switchCommand := ""

	if bot.Pos.Lane.End < lane {
		switchCommand = "Right"
	} else {
		switchCommand = "Left"
	}

	return switchCommand
}
