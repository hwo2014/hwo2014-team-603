package main

type ApproxAccel struct {
	Points []Point
	Line   Line
}

type Line struct {
	A float64
	B float64
}

type Point struct {
	X float64
	Y float64
}

func (l Line) Eval(x float64) float64 {
	return l.A*x + l.B
}

type Oscillator struct {
	Ready bool
	A     float64
	B     float64
	F     func(float64, float64, float64) float64
}

func (o Oscillator) Eval(x float64, dx float64, v float64, r float64, a float64) float64 {
	if !o.Ready {
		return 0
	}

	return o.F(v, r, a) + dx*(o.A+o.B*v) + x*(o.B*v)
}

func GetEstimateLine(x1, x2, y1, y2 float64) Line {
	if x1-x2 != 0 {
		a := (y2 - y1) / (x2 - x1)
		b := (x2*y1 - x1*y2) / (x2 - x1)

		return Line{a, b}
	} else {
		return Line{0, 0}
	}
}

func LinearModelFit(pts []Point) Line {
	cnt := float64(len(pts))
	if cnt == 0 {
		return Line{}
	}

	// special case
	if cnt == 1 {
		return Line{0, pts[0].Y}
	}

	xBar := 0.0
	yBar := 0.0
	for _, p := range pts {
		xBar += p.X
		yBar += p.Y
	}
	xBar /= cnt
	yBar /= cnt

	top := 0.0
	bot := 0.0
	for _, p := range pts {
		top += (p.X - xBar) * (p.Y - yBar)
		bot += (p.X - xBar) * (p.X - xBar)
	}
	a := top / bot
	b := yBar - a*xBar

	return Line{a, b}
}
