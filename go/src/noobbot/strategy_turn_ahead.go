package main

import ()

type StrategyTurnAhead struct {
	Bot   *Noob
	Debug bool
}

func (strategy StrategyTurnAhead) Name() string {
	return "turn_ahead"
}

func (strategy StrategyTurnAhead) Check() bool {
	bot := strategy.Bot

	nextTurnIndex, nextTurnDistance := bot.GetNextTurnInfo(bot.Pos.Index,
		bot.Pos.Distance)

	nextPiece := bot.Track.Pieces[nextTurnIndex]

	maximalTurnVelocity := bot.GetMaximalPreTurnVelocity(nextPiece.Radius,
		nextPiece.Angle, bot.Angle)

	nextTickPosition := bot.GetPositionAfterTicks(1, 0, bot.Velocity, 1)

	nextTickFinalVelocity := bot.GetVelocityAtPosition(nextTurnDistance-
		nextTickPosition, bot.Velocity, 0)

	if nextTickFinalVelocity <= maximalTurnVelocity {
		return false
	}

	return true
}

func (strategy StrategyTurnAhead) GetActions() (float64, string, bool) {
	return 0, "", false
}
