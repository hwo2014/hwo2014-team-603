package main

import (
// "log"
)

type Opponent struct {
	Bot        *Noob
	CarId      CarId
	Dimensions CarDimensions
	Pos        CarPosition
	History    []CarPosition
}

func (opponent Opponent) GetVelocity() float64 {
	if len(opponent.History) == 0 {
		return 0
	}

	if len(opponent.History) == 1 {
		return opponent.History[0].Position.Distance
	}

	lastPosition := opponent.History[len(opponent.History)-1].Position
	prevPosition := opponent.History[len(opponent.History)-2].Position

	if lastPosition.Index != prevPosition.Index {
		return lastPosition.Distance +
			opponent.Bot.GetPieceLength(prevPosition.Index) -
			prevPosition.Distance
	}

	return lastPosition.Distance - prevPosition.Distance
}
