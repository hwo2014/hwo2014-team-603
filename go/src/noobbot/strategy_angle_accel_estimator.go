package main

import (
	"log"
	"math"
)

type StrategyAngleAccelEstimation struct {
	Bot               *Noob
	EstimationPoints  []Point
	InTurn            bool
	TurnTick          int
	BasicEstimateMade bool
	SufficientData    bool
	Model             Oscillator
	EstNextAngleAccel float64
	Threshold         float64
	PrevRealF         float64
	SlipF             float64
	SlipB             float64
}

func (s *StrategyAngleAccelEstimation) Name() string {
	return "angle_accel_estimator"
}

func (s *StrategyAngleAccelEstimation) Check() bool {
	estimationDepth := 100

	if s.Bot.TurnRadius > 0 {
		if !s.InTurn {
			s.TurnTick = 0
		}
		s.InTurn = true
	} else {
		s.InTurn = false
	}

	if len(s.EstimationPoints) >= estimationDepth/3 {
		s.SufficientData = true
	}

	if s.TurnTick > 0 && s.InTurn && s.BasicEstimateMade {
		prevEst := s.Bot.EstNextAngleAccel
		estError := prevEst - s.Bot.AngleAccel
		hist := s.Bot.History[s.Bot.Tick-1]
		realF := s.Model.F(s.Bot.Velocity,
			hist.TurnRadius,
			hist.Piece.Angle)
		if math.Abs(estError) > 1e-6 {
			realF -= estError
		}
		log.Println("estError", estError)

		estF := 0.0
		//log.Println(math.Sqrt(s.SlipF * s.Bot.TurnRadius))
		if s.Bot.Velocity >= s.Bot.calcSlipVelocity(s.Bot.TurnRadius) {
			estF = s.Bot.Velocity * s.Bot.Velocity / s.Bot.TurnRadius
		}

		if math.Abs(realF) > 1e-4 { /*&& s.Threshold > 0 {*/
			s.EstimationPoints = append(s.EstimationPoints, Point{
				estF,
				math.Abs(realF),
			})
		}

		length := len(s.EstimationPoints)
		if length > estimationDepth {
			s.EstimationPoints = s.EstimationPoints[length-estimationDepth : length]
		}

		line := LinearModelFit(s.EstimationPoints)

		s.Model.F = func(v float64, r float64, a float64) float64 {
			//log.Println(v, r, a)
			if r == 0 {
				return 0
			}

			if v <= s.Bot.calcSlipVelocity(r) {
				return 0.0
			}

			estF := v * v / r
			return line.Eval(estF) * math.Abs(a) / a
		}
	}

	if s.TurnTick == 2 && !s.BasicEstimateMade {
		if !s.basicEstimation() {
			s.TurnTick = 0
		} else {
			s.BasicEstimateMade = true
		}
	}

	if s.InTurn {
		s.TurnTick += 1
	}

	s.Bot.AngleAccelEst = s.Model

	if s.InTurn && (!s.BasicEstimateMade || !s.SufficientData) {
		log.Printf("[strategy] in_turn_tick: %d", s.TurnTick)
		return true
	} else {
		return false
	}
}

func (s *StrategyAngleAccelEstimation) GetActions() (float64, string, bool) {
	// @TODO: fix for large radiuses
	if s.Bot.TurnRadius > 0 {
		// @TODO: fix magic coeff
		if s.TurnTick < 3 {
			return s.Bot.Velocity / 10.0, "", false
		} else {
			if !s.SufficientData {
				return s.Bot.Velocity/10.0 + 0.01, "", false
			} else {
				return 0.0, "", false
			}
		}
	} else {
		return 0.75, "", false
	}
}

func (s *StrategyAngleAccelEstimation) basicEstimation() bool {
	acc1 := s.Bot.History[s.Bot.Tick-2].AngleAccel
	s.Model.F = func(float64, float64, float64) float64 {
		return acc1
	}

	if acc1 == 0 {
		return false
	}

	b := 180.0 / math.Pi * s.Bot.Velocity / s.Bot.TurnRadius
	slipB := b - acc1
	vt := math.Pi / 180.0 * slipB * s.Bot.TurnRadius
	slipF := vt * vt / s.Bot.TurnRadius

	s.Bot.SlipForce = slipF

	log.Println("SLIPV", vt)

	acc2 := s.Bot.History[s.Bot.Tick-1].AngleAccel
	vel2 := s.Bot.History[s.Bot.Tick-2].AngleVelocity

	acc3 := s.Bot.AngleAccel
	vel3 := s.Bot.History[s.Bot.Tick-1].AngleVelocity
	ang3 := s.Bot.History[s.Bot.Tick-2].Angle

	c1 := (acc2 - acc1) / vel2
	c2 := (acc3 - acc1 - vel3*c1) / ang3

	s.Model.A = c1 - c2
	s.Model.B = c2 / s.Bot.Velocity

	s.Model.Ready = true

	return true
}
