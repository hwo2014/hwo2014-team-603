package main

import (
	"fmt"
)

// Black 30  40
// Red 31  41
// Green 32  42
// Yellow  33  43
// Blue  34  44
// Magenta 35  45
// Cyan  36  46
// White 37  47
func Scolorf(color string, str string, args ...interface{}) string {
	colors := map[string]int{
		"black":   30,
		"red":     31,
		"green":   32,
		"yellow":  33,
		"blue":    34,
		"magenta": 35,
		"cyan":    36,
		"white":   37,
	}

	colorNum := 30
	if num, ok := colors[color]; ok {
		colorNum = num
	}

	return fmt.Sprintf("\033[%dm%s\033[0m", colorNum, fmt.Sprintf(str, args...))
}
