package main

import (
	"math"
)

func (bot Noob) GetMaximalVelocityOnTurn(radius float64,
	angle float64, botAngle float64) float64 {

	// if math.Abs(botAngle) > 45 {
	//   return 10
	// }

	speed := 1 + radius/25.0 + (1 - math.Abs(botAngle/1.5)/40.0) - 2 // hack it please...
	if speed < 3 {
		speed = 3
	}

	return speed
}

func (bot Noob) GetMaximalPreTurnVelocity(radius float64,
	angle float64, botAngle float64) float64 {

	if math.Abs(botAngle) > 45 {
		return 10
	}

	speed := 3 + radius/25.0 - math.Abs(angle+botAngle)/15 - 2 // hack it please...
	if speed < 3 {
		speed = 3
	}

	return speed
}

func (bot Noob) GetVelocityAtPosition(finalPosition float64,
	currentVelocity float64, throttle float64) float64 {

	for currentPosition := 0.0; currentPosition < finalPosition; {
		currentVelocity += bot.calcNextAcceleration(currentVelocity, throttle)
		currentPosition += currentVelocity

		if currentVelocity < 0.0005 {
			return 0
		}
	}

	return currentVelocity
}

func (bot Noob) GetPositionAfterTicks(ticks int, currentPosition float64,
	currentVelocity float64, throttle float64) float64 {

	for tick := 0; tick < ticks; tick += 1 {
		currentVelocity += bot.calcNextAcceleration(currentVelocity, throttle)
		currentPosition += currentVelocity
	}

	return currentPosition
}

func (bot Noob) GetVelocityAfterTicks(ticks int, currentVelocity float64,
	throttle float64) float64 {

	for tick := 0; tick < ticks; tick += 1 {
		currentVelocity += bot.calcNextAcceleration(currentVelocity, throttle)
	}

	return currentVelocity
}

func (bot Noob) GetDistanceForVelocity(targetVelocity float64,
	currentVelocity float64, throttle float64) float64 {

	shouldGrowth := bot.calcNextAcceleration(currentVelocity, throttle) > 0
	distance := 0.0

	for tick := 0; ; tick += 1 {
		currentVelocity += bot.calcNextAcceleration(currentVelocity, throttle)
		if shouldGrowth {
			if currentVelocity > targetVelocity {
				break
			}
		} else {
			if currentVelocity < targetVelocity {
				break
			}
		}

		distance += currentVelocity

		if tick > 999 {
			return 0
		}
	}

	return distance
}

// probably somebody will need this func

// func (bot Noob) GetTicksForVelocity(targetVelocity float64,
// 	currentVelocity float64, throttle float64) int {

// 	shouldGrowth := targetVelocity < currentVelocity

// 	ticks := 0
// 	for ticks = 0; ; ticks += 1 {
// 		currentVelocity += bot.calcNextAcceleration(currentVelocity, throttle)
// 		if shouldGrowth {
// 			if targetVelocity > currentVelocity {
// 				return ticks
// 			} else targetVelocity < currentVelocity {
// 				return ticks
// 			}
// 		}

// 		if ticks > 999 {
// 			return 0
// 		}
// 	}

// 	return ticks
// }

func (bot Noob) GetNextTurnInfo(currenPieceIndex int,
	currenPiecePosition float64) (int, float64) {

	startIndex := currenPieceIndex + 1
	piecesMaxIndex := len(bot.Track.Pieces) - 1
	if startIndex > piecesMaxIndex {
		startIndex = 0
	}

	distance := 0.0
	pieceIndex := startIndex
	for ; pieceIndex != currenPieceIndex; pieceIndex += 1 {
		if pieceIndex > piecesMaxIndex {
			pieceIndex = 0
		}

		if bot.Track.Pieces[pieceIndex].Radius != 0 {
			break
		}

		distance += bot.GetPieceLength(pieceIndex)
	}

	return pieceIndex, distance
}

func (bot Noob) GetNextSwitchInfo(currenPieceIndex int,
	currenPiecePosition float64) (int, float64) {

	startIndex := currenPieceIndex + 1
	piecesMaxIndex := len(bot.Track.Pieces) - 1
	if startIndex > piecesMaxIndex {
		startIndex = 0
	}

	distance := 0.0
	pieceIndex := startIndex
	for ; pieceIndex != currenPieceIndex; pieceIndex += 1 {
		if pieceIndex > piecesMaxIndex {
			pieceIndex = 0
		}

		if bot.Track.Pieces[pieceIndex].Switch == true {
			break
		}

		distance += bot.GetPieceLength(pieceIndex)
	}

	return pieceIndex, distance
}

func (bot Noob) GetNextPiece(currenPieceIndex int) (int, Piece) {
	currenPieceIndex += 1
	piecesMaxIndex := len(bot.Track.Pieces) - 1
	if currenPieceIndex > piecesMaxIndex {
		currenPieceIndex = 0
	}

	return currenPieceIndex, bot.Track.Pieces[currenPieceIndex]
}

func (bot Noob) GetPieceLength(pieceIndex int) float64 {
	piece := bot.Track.Pieces[pieceIndex]
	if piece.Length > 0 {
		return piece.Length
	}

	return bot.GetTurnLength(piece.Radius, piece.Angle, bot.Pos.Lane.End)
}

func (bot Noob) GetTurnLength(radius float64, angle float64,
	laneIndex int) float64 {
	distanceFromCenter := 0.0

	for _, lane := range bot.Track.Lanes {
		if lane.Index == laneIndex {
			distanceFromCenter = lane.DistanceFromCenter
		}
	}

	if angle > 0 {
		distanceFromCenter *= -1
	}

	radius += distanceFromCenter // fucking huck
	return math.Abs(angle) / 180 * math.Pi * radius
}

func (bot Noob) GetAngleAcceleration(botAngleAcceleration float64,
	botAngle float64, botVelocity float64, turnRadius float64,
	throttle float64) float64 {

	return botAngleAcceleration + botAngle + botVelocity + turnRadius +
		throttle*(2^2/2)
}

func (bot Noob) GetDistanceTo(pieceIndex int, distance float64) float64 {
	for currentPieceIndex := bot.Pos.Index; currentPieceIndex != pieceIndex; {
		distance += bot.GetPieceLength(currentPieceIndex)
		currentPieceIndex += 1

		if currentPieceIndex > len(bot.Track.Pieces)-1 {
			currentPieceIndex = 0
		}
	}

	return distance - bot.Pos.Distance
}

func (bot Noob) isNextSwitchOnNextTick() bool {
	_, nextPiece := bot.GetNextPiece(bot.Pos.Index)
	if nextPiece.Switch == false {
		return false
	}

	pieceLength := bot.GetPieceLength(bot.Pos.Index)
	distanceToNextPiece := pieceLength - bot.Pos.Distance
	if distanceToNextPiece >= bot.Velocity+2 { // ?
		return false
	}

	return true
}

func (bot Noob) GetSwitchForLane(lane int) string {
	switchCommand := ""

	if bot.Pos.Lane.End < lane {
		switchCommand = "Right"
	} else {
		switchCommand = "Left"
	}

	return switchCommand
}

func (bot Noob) GetThrottleForVelocity(maximalVelocity float64) float64 {
	for throttle := 1.0; throttle > 0; throttle -= 0.05 {
		newVelocity := bot.GetVelocityAfterTicks(1, bot.Velocity, throttle)
		if newVelocity < maximalVelocity {
			return throttle
		}
	}

	return 0
}

func (bot Noob) GetClosestOpponent(lane int, lookForward bool) (float64,
	*Opponent) {

	minDistance := 0.0
	minOpponentIndex := CarId{"", ""}
	for opponentIndex, opponent := range bot.Opponents {
		if lane != opponent.Pos.Position.Lane.End {
			continue
		}

		dist := bot.GetDistanceTo(opponent.Pos.Position.Index,
			opponent.Pos.Position.Distance)

		dist -= (opponent.Dimensions.Length - opponent.Dimensions.FlagPos)
		dist -= bot.Car.Dimensions.FlagPos

		if lookForward {
			if dist < 0 {
				continue
			}
		} else {
			if dist > 0 {
				continue
			}
		}

		dist = math.Abs(dist)

		if dist < minDistance || minDistance == 0 {
			minDistance = dist
			minOpponentIndex = opponentIndex
		}
	}

	if minDistance == 0 {
		return 0, nil
	}

	return minDistance, bot.Opponents[minOpponentIndex]
}

func (bot Noob) getClosestOpponentBeforeTwoSwitches(lane int) (float64,
	*Opponent) {

	opponentDistance, opponent := bot.GetClosestOpponent(lane, true)
	if opponent == nil {
		return 0, nil
	}

	switchIndex, switchDistance := bot.GetNextSwitchInfo(bot.Pos.Index,
		bot.Pos.Distance)

	_, nextSwitchAfterDistance := bot.GetNextSwitchInfo(
		switchIndex, 0)

	// if opponentDistance < switchDistance {
	// 	return 0, nil
	// }

	if opponentDistance > switchDistance+nextSwitchAfterDistance {
		return 0, nil
	}

	return opponentDistance, opponent
}

func (bot Noob) LaneInfo(lane int) (bool, *Opponent, float64) {
	if lane < 0 || lane > len(bot.Track.Lanes)-1 {
		return false, nil, 0
	}

	laneOpponentDistance, laneOpponent :=
		bot.getClosestOpponentBeforeTwoSwitches(lane)

	return true, laneOpponent, laneOpponentDistance
}

func (bot Noob) IsLaneBlockedForTurnByOpponent(lane int) bool {
	// 20 + Length is safe turn distance

	frontOpponentDistance, frontOpponent := bot.GetClosestOpponent(lane, true)
	if frontOpponent != nil && frontOpponentDistance < 5 {
		return true
	}

	rearOpponentDistance, rearOpponent := bot.GetClosestOpponent(lane, false)
	if rearOpponent != nil && rearOpponentDistance <
		bot.Car.Dimensions.Length/8+rearOpponent.Dimensions.Length {

		return true
	}

	return false
}
