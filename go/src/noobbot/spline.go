package main

type Spline struct {
	parts []PartOfSpline
}

type PartOfSpline struct {
	a, b, c, d, x float64
}

func (s *Spline) Build(x []float64, y []float64) {
	if len(x) != len(y) {
		return
	}

	n := len(x)
	for i := 1; i < n; i++ {
		if x[i-1] == x[i] && y[i-1] == y[i] {
			x = append(x[:i], x[i+1:]...)
			y = append(y[:i], y[i+1:]...)
			n--
		}
	}

	if n < 3 {
		return
	}

	s.parts = make([]PartOfSpline, n)
	for i := 0; i < n; i++ {
		s.parts[i].x = x[i]
		s.parts[i].a = y[i]
	}
	s.parts[0].c = 0.0

	alpha := make([]float64, n-1)
	beta := make([]float64, n-1)
	var a, b, c, f, hi, hi1, z float64
	alpha[0] = 0.0
	beta[0] = 0.0
	for i := 1; i < n-1; i++ {
		hi = x[i] - x[i-1]
		hi1 = x[i+1] - x[i]
		a = hi
		c = 2.0 * (hi + hi1)
		b = hi1
		f = 6.0 * ((y[i+1]-y[i])/hi1 - (y[i]-y[i-1])/hi)
		z = (a*alpha[i-1] + c)
		alpha[i] = -b / z
		beta[i] = (f - a*beta[i-1]) / z
	}

	s.parts[n-1].c = (f - a*beta[n-2]) / (c + a*alpha[n-2])

	for i := n - 2; i > 0; i-- {
		s.parts[i].c = alpha[i]*s.parts[i+1].c + beta[i]
	}

	for i := n - 1; i > 0; i-- {
		hi := x[i] - x[i-1]
		s.parts[i].d = (s.parts[i].c - s.parts[i-1].c) / hi
		s.parts[i].b = hi*(2.*s.parts[i].c+s.parts[i-1].c)/6.0 + (y[i]-y[i-1])/hi
	}
}

func (s *Spline) Eval(x float64) float64 {
	var part PartOfSpline
	n := len(s.parts)
	if n < 1 {
		return 0.0
	}

	if x <= s.parts[0].x {
		part = s.parts[0]
	} else if x >= s.parts[n-1].x {
		part = s.parts[n-1]
	} else {
		i := 0
		j := n - 1
		for i+1 < j {
			k := i + (j-i)/2
			if x <= s.parts[k].x {
				j = k
			} else {
				i = k
			}
		}
		part = s.parts[j]
	}

	dx := (x - part.x)
	return part.a + (part.b+(part.c/2.0+part.d*dx/6.0)*dx)*dx
}
