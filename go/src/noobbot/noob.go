package main

import (
	"log"
	"math"
)

var _ = math.E

type Noob struct {
	Tick     int
	Car      Car
	Track    Track
	Accel    float64
	Velocity float64
	Angle    float64
	Pos      PiecePosition
	Piece    Piece
	Throttle float64
	Boost    Boost
	Turbo    bool
	Switch   string
	Lab      interface{}
	History  map[int]HistoryElement

	AccelEst      Line
	AngleAccelEst Oscillator

	SlipForce float64

	TurnRadius       float64
	AngleVelocity    float64
	AngleAccel       float64
	StrategySelecter *StrategySelecter

	EstNextAngleAccel float64

	Opponents      map[CarId]*Opponent
	CarsDimensions map[CarId]CarDimensions

	TurboActivatedAt int
	TurboDuration    int
}

type HistoryElement struct {
	Throttle         float64
	Pos              PiecePosition
	Velocity         float64
	Accel            float64
	AccelErr         float64
	Angle            float64
	Piece            Piece
	TurnRadius       float64
	AngleVelocity    float64
	AngleAccel       float64
	CentrifugalCoeff float64
	CentrifugalForce float64
}

func (b *Noob) SetTick(tick int) {
	b.Tick = tick
}

func (b *Noob) Join(bi BotId) {
	log.Println("[noob] joined")
}

func (b *Noob) GameInit(gi GameInit) {
	log.Println("[noob] game init")
	// @todo We need to sort lanes here from minimal distance to maximal and
	// then map it in strategies
	b.Track = gi.Race.Track

	b.Opponents = make(map[CarId]*Opponent)
	b.CarsDimensions = make(map[CarId]CarDimensions)

	for _, car := range gi.Race.Cars {
		log.Println(car)
		if car.Id == b.Car.Id {
			b.Car = car
		}

		b.CarsDimensions[car.Id] = car.Dimensions
	}

	b.History = make(map[int]HistoryElement)

	log.Printf("[noob] my car info: %#v", b.Car)
}

func (b *Noob) GameStart(gs GameStart) {
	log.Println("[noob] game start")
}

func (b *Noob) GameEnd(ge GameEnd) {
	log.Println("[noob] game end")
}

func (b *Noob) YourCar(c CarId) {
	log.Printf("[noob] my car is %s", c.Color)
	b.Car.Id = c
}

func (b *Noob) TurboAvailable(boost Boost) {
	log.Println(Scolorf("green", "[noob] TURBO AVAILABLE: %d", b.Pos.Index))
	b.Boost = boost
}

func (b *Noob) CarPositions(cp CarPositions) Msg {
	if b.Tick == 0 {
		return nil
	}

	b.findAngleAndPos(cp)
	b.calcVelocity()
	b.calcAccel()
	b.calcMyPiece()

	log.Println((Scolorf("magenta", "[noob] tick %d piece %d lane %d", b.Tick,
		b.Pos.Index, b.Pos.Lane.End)))

	strategy := b.StrategySelecter.Select()
	localThrottle, localSwitch, localTurbo := -1.0, "", false
	if strategy != nil {
		localThrottle, localSwitch, localTurbo = strategy.GetActions()
	}

	var msg Msg = nil

	b.Turbo = localTurbo
	if localTurbo == true {
		msg = Turbo("")
	}

	b.Switch = localSwitch
	if localSwitch != "" {
		msg = Switch(b.Switch)
	}

	if msg == nil && localThrottle >= 0 {
		b.Throttle = localThrottle
		msg = Throttle(b.Throttle)
	}

	if msg == nil {
		msg = Ping{}
	}

	accel := b.calcNextAcceleration(b.Velocity, b.Throttle)
	if b.AngleAccelEst.Ready {
		b.EstNextAngleAccel = b.AngleAccelEst.Eval(
			b.History[b.Tick-1].Angle,
			b.AngleVelocity,
			b.Velocity+accel,
			b.TurnRadius,
			b.Piece.Angle)

		log.Println(Scolorf("blue", "[noob] est_next_angle_accel: %f",
			b.EstNextAngleAccel))
	}

	log.Println(Scolorf("yellow", "[noob] strategy: %s", strategy.Name()))
	// log.Println(Scolorf("yellow", "[noob] piece: %d", b.Pos.Index))
	log.Println(Scolorf("green", "[noob] velocity: %f", b.Velocity))
	// log.Println(Scolorf("green", "[noob] accel: %f", b.Accel))
	log.Println(Scolorf("green", "[noob] angle: %f", b.Angle))
	log.Println(Scolorf("green", "[noob] angle_vel: %f", b.AngleVelocity))
	log.Println(Scolorf("green", "[noob] angle_accel: %f", b.AngleAccel))
	log.Println(Scolorf("red", "[noob] throttle: %f", b.Throttle))
	// log.Println(Scolorf("green", "[noob] turn_radius: %f", b.TurnRadius))
	// log.Println(Scolorf("green", "[noob] turn_angel: %f", b.Piece.Angle))

	//can be calculated for any velocity/throttle
	// nextAccelApprox := b.calcNextAcceleration(b.Velocity, b.Throttle)
	// log.Println(Scolorf("blue", "[noob] est_next_accel: %f",
	// 	nextAccelApprox))

	b.keepHistoryGoing()

	return msg
}

func (b *Noob) Crash(c Crash) {
	log.Println("[noob] CRASH: ", c)
}

func (b *Noob) Error(e Error) {
	log.Println("[noob] ERROR: ", e)
}

func (b *Noob) calcMyPiece() {
	b.Piece = b.Track.Pieces[b.Pos.Index]
	if b.Piece.Radius > 0 {
		laneDist := b.Track.Lanes[b.Pos.Lane.Start].DistanceFromCenter
		if b.Piece.Angle > 0 {
			b.TurnRadius = b.Piece.Radius - laneDist
		} else {
			b.TurnRadius = b.Piece.Radius + laneDist
		}
	} else {
		b.TurnRadius = 0
	}
}

func (b *Noob) calcNextAcceleration(velocity, throttle float64) float64 {
	// magic from calcNextAcceleration
	tCoeff := 1.0 / 5.0
	acceleration := b.AccelEst.Eval(velocity) + tCoeff*(throttle-0.5)

	if b.TurboDuration > 0 && b.Tick < b.TurboActivatedAt+b.TurboDuration {
		if acceleration > 0 {
			acceleration *= b.Boost.TurboFactor
		}
	}

	return acceleration
}

func (b *Noob) calcVelocity() {
	if b.Tick <= 1 {
		b.Velocity = 0.0
	} else {
		//prevVelocity := b.History[b.Tick-1].Velocity
		prevPos := b.History[b.Tick-1].Pos

		if prevPos.Index == b.Pos.Index {
			b.Velocity = b.Pos.Distance - prevPos.Distance
		} else {
			// @FIXME calc arc length, now velocity is not calculated in between arcs
			if b.Track.Pieces[prevPos.Index].Length > 0 {
				b.Velocity = b.Track.Pieces[prevPos.Index].Length -
					prevPos.Distance + b.Pos.Distance
			}
		}
	}
}

func (b *Noob) calcSlipVelocity(radius float64) float64 {
	return math.Sqrt(radius * b.SlipForce)
}

func (b *Noob) findAngleAndPos(cp CarPositions) {
	for _, p := range cp {
		if p.Id == b.Car.Id {
			b.Pos = p.Position
			b.AngleAccel = (p.Angle - b.Angle) - b.AngleVelocity
			b.AngleVelocity = p.Angle - b.Angle
			b.Angle = p.Angle
		} else {
			opponent, exist := b.Opponents[p.Id]
			if exist {
				opponent.History = append(b.Opponents[p.Id].History, p)
				historyLength := len(opponent.History)

				// log.Println(p.Id, p)

				// memory safe history
				if historyLength > 10 {
					opponent.History = opponent.History[historyLength-10 : historyLength]
				}

				opponent.Pos = p
			} else {
				b.Opponents[p.Id] = &Opponent{
					Bot:        b,
					Pos:        p,
					History:    []CarPosition{p},
					CarId:      p.Id,
					Dimensions: b.CarsDimensions[p.Id],
				}
			}
		}
	}
}

func (b *Noob) keepHistoryGoing() {
	b.History[b.Tick] = HistoryElement{
		Pos:           b.Pos,
		Throttle:      b.Throttle,
		Velocity:      b.Velocity,
		Accel:         b.Accel,
		Angle:         b.Angle,
		AngleVelocity: b.AngleVelocity,
		AngleAccel:    b.AngleAccel,
		Piece:         b.Piece,
		TurnRadius:    b.TurnRadius,
	}
}

func (b *Noob) calcAccel() {
	if b.Tick <= 1 {
		b.Accel = 0
	} else {
		b.Accel = b.Velocity - b.History[b.Tick-1].Velocity
	}
}
