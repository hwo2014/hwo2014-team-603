package main

// usage:
//
// 1. Implement interface Strategy
// 2. Add your own strategies at CreateStrategySelecter
// 3. Strategy checks and executes in the order of adding - earlier added - earlier checked and executed
// 3. Profit

type Strategy interface {
	Name() string // only for debug
	Check() bool
	GetActions() (float64, string, bool) // throttle, boost
}

type StrategySelecter struct {
	Strategies []Strategy
}

func (selecter StrategySelecter) Select() Strategy {
	for _, strategy := range selecter.Strategies {
		if strategy.Check() {
			return strategy
		}
	}

	return nil
}

func CreateStrategySelecter(bot *Noob) *StrategySelecter {
	return &StrategySelecter{
		Strategies: []Strategy{
			&StrategyAccelEstimation{Bot: bot},
			// &StrategyAngleAccelEstimation{Bot: bot},
			// &StrategyTurnOptimal{Bot: bot},
			&StrategyOverrun{Bot: bot},
			&StrategySwitchLaneBeforeTurn{Bot: bot},
			&StrategyOpponentAhead{Bot: bot},
			&StrategyTurnGoing{Bot: bot},
			&StrategyTurnAhead{Bot: bot},
			&StrategyTurboOnLine{Bot: bot},
			&StrategyFullThrottle{Bot: bot, Throttle: 1},
		},
	}
}
