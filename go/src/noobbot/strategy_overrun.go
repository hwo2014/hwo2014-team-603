package main

import (
	"math"
)

type StrategyOverrun struct {
	Bot             *Noob
	maximalVelocity float64
	BestLane        int
}

func (strategy StrategyOverrun) Name() string {
	return "overrun"
}

func (strategy *StrategyOverrun) Check() bool {
	bot := strategy.Bot

	if !bot.isNextSwitchOnNextTick() {
		return false
	}

	opponentDistance, opponent := bot.getClosestOpponentBeforeTwoSwitches(
		bot.Pos.Lane.End)

	if opponent == nil {
		return false
	}

	bestLaneFound, bestLane := strategy.getBestLane(opponentDistance)
	if !bestLaneFound {
		return false
	}

	strategy.BestLane = bestLane
	return true
}

func (strategy StrategyOverrun) GetActions() (float64, string, bool) {
	return 0, strategy.Bot.GetSwitchForLane(strategy.BestLane), false
}

func (strategy StrategyOverrun) getBestLane(opponentDistance float64) (bool,
	int) {

	bot := strategy.Bot

	prevLane := bot.Pos.Lane.End - 1
	prevLaneAvailable, prevLaneOpponent, prevLaneOpponentDistance :=
		bot.LaneInfo(prevLane)

	if prevLaneAvailable && bot.IsLaneBlockedForTurnByOpponent(prevLane) {
		prevLaneAvailable = false
	}

	nextLane := bot.Pos.Lane.End + 1
	nextLaneAvailable, nextLaneOpponent, nextLaneOpponentDistance :=
		bot.LaneInfo(nextLane)

	if nextLaneAvailable && bot.IsLaneBlockedForTurnByOpponent(nextLane) {
		nextLaneAvailable = false
	}

	if prevLaneAvailable && nextLaneAvailable && prevLaneOpponent == nil &&
		nextLaneOpponent == nil {

		return bot.getBestLaneForTurn()
	}

	if prevLaneAvailable && prevLaneOpponent == nil {
		return true, prevLane
	}

	if nextLaneAvailable && nextLaneOpponent == nil {
		return true, nextLane
	}

	if prevLaneAvailable && nextLaneAvailable && opponentDistance > math.Min(
		nextLaneOpponentDistance, prevLaneOpponentDistance) {

		if prevLaneOpponentDistance > nextLaneOpponentDistance {
			return true, prevLane
		} else if prevLaneAvailable {
			return true, nextLane
		}
	}

	if !prevLaneAvailable && nextLaneAvailable && opponentDistance <
		nextLaneOpponentDistance {

		return true, nextLane
	}

	if prevLaneAvailable && !nextLaneAvailable && opponentDistance <
		prevLaneOpponentDistance {

		return true, prevLane
	}

	return false, 0
}
