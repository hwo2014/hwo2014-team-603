package main

import (
	// "log"
	"math"
)

type StrategyTurnGoing struct {
	Bot             *Noob
	maximalVelocity float64
}

func (strategy StrategyTurnGoing) Name() string {
	return "turn_going"
}

func (strategy *StrategyTurnGoing) Check() bool {
	bot := strategy.Bot

	currentPiece := bot.Track.Pieces[bot.Pos.Index]
	if currentPiece.Radius == 0 {
		return false
	}

	strategy.maximalVelocity = bot.GetMaximalVelocityOnTurn(currentPiece.Radius,
		currentPiece.Angle, bot.Angle)

	return true
}

func (strategy StrategyTurnGoing) GetActions() (float64, string, bool) {
	bot := strategy.Bot

	if math.Abs(bot.Angle) > 45 {
		return 1, "", false
	}

	return bot.GetThrottleForVelocity(strategy.maximalVelocity), "", false
}
