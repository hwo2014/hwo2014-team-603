package main

type StrategyOpponentAhead struct {
	Bot                     *Noob
	ClosestOpponentDistance float64
	ClosestOpponentVelocity float64
}

func (strategy StrategyOpponentAhead) Name() string {
	return "strategy_opponent_ahead"
}

func (strategy *StrategyOpponentAhead) Check() bool {
	bot := strategy.Bot
	distance, opponent := bot.GetClosestOpponent(bot.Pos.Lane.End, true)
	if opponent == nil {
		return false
	}

	distance -= 5 // safe is 5
	opponentVelocity := opponent.GetVelocity()

	if distance > 10 {

		if bot.Velocity < opponentVelocity {
			return false
		}

		breakDistance := bot.GetDistanceForVelocity(opponentVelocity,
			bot.Velocity, 0)

		if distance > breakDistance {
			return false
		}
	}

	strategy.ClosestOpponentDistance = distance
	strategy.ClosestOpponentVelocity = opponentVelocity
	return true
}

func (strategy StrategyOpponentAhead) GetActions() (float64, string, bool) {
	maximalVelocity := strategy.ClosestOpponentVelocity

	if strategy.ClosestOpponentDistance < 5 {
		maximalVelocity -= 0.5
	}

	return strategy.Bot.GetThrottleForVelocity(maximalVelocity), "", false
}
