package main

import (
	"log"
)

type StrategyTurnOptimal struct {
	Bot             *Noob
	maximalVelocity float64
	Throttle        float64
}

func (strategy StrategyTurnOptimal) Name() string {
	return "turn_optimal"
}

func (strategy *StrategyTurnOptimal) Check() bool {
	bot := strategy.Bot

	if !bot.AngleAccelEst.Ready {
		log.Println("Strategy rejected (1)")
		return false
	}

	nextIndex := bot.Pos.Index + 10
	nextIndex = nextIndex % (len(bot.Track.Pieces) - 1)

	log.Println("LOOKING FOR THROTTLE", nextIndex)
	log.Println("LOOKING FOR THROTTLE AT DISTANCE", bot.GetDistanceTo(nextIndex, 0))
	found, throttle := bot.getOptimalTrottle(nextIndex, 0)
	if !found {
		log.Println("Strategy rejected (2)")
		return false
	}

	strategy.Throttle = throttle
	log.Println("RECOMMENDED THROTTLE", throttle)
	return true
}

func (strategy StrategyTurnOptimal) GetActions() (float64, string, bool) {
	return strategy.Throttle, "", false
}

func (bot Noob) getOptimalTrottle(targetPieceIndex int,
	targetPieceDistance float64) (bool, float64) {

	previousAngle := bot.History[bot.Tick-1].Angle
	currentAngle := bot.Angle
	currentAngleVelocity := bot.AngleVelocity
	currentVelocity := bot.Velocity
	currentTurnRadius := bot.TurnRadius
	currentPieceAngle := bot.Piece.Angle

	// bot.calcSlipVelocity(radius)
	return bot.getOptimalTrottleRecursive(
		bot.Pos,
		targetPieceIndex,
		targetPieceDistance,
		previousAngle,
		currentAngle,
		currentAngleVelocity,
		currentVelocity,
		currentTurnRadius,
		currentPieceAngle,
		1,
	)
}

func (bot Noob) getOptimalTrottleRecursive(
	currentPosition PiecePosition,
	targetPieceIndex int,
	targetPieceDistance float64,
	previousAngle float64,
	currentAngle float64,
	currentAngleVelocity float64,
	currentVelocity float64,
	currentTurnRadius float64,
	currentPieceAngle float64,
	depth int,
) (bool, float64) {

	log.Println("CUR SIMULATION POSITION", depth, currentPosition.Index,
		currentPosition.Distance)

	found := false

	currentThrottle := 1.0

	for !found && currentThrottle > 0 {
		// log.Println("CUR THROTTLE", found, currentThrottle, currentPosition.Index,
		// 	currentPosition.Distance)

		angle, angleVelocity, velocity := bot.calcParametersAtNextTick(
			previousAngle,
			currentAngle,
			currentAngleVelocity,
			currentVelocity,
			currentTurnRadius,
			currentPieceAngle,
			currentThrottle,
		)

		if angle > 55 {
			currentThrottle -= 0.5
			continue
		}

		previousAngle = currentAngle
		currentAngle = angle
		currentAngleVelocity = angleVelocity
		currentVelocity = velocity

		currentPosition.Distance += currentVelocity
		if currentPosition.Distance > bot.GetPieceLength(currentPosition.Index) {
			currentPieceIndex, currentPiece := bot.GetNextPiece(currentPosition.Index)

			currentPosition.Index = currentPieceIndex
			currentPosition.Distance = 0

			currentTurnRadius = currentPiece.Radius
			currentPieceAngle = currentPiece.Angle
		}

		if currentPosition.Index == targetPieceIndex && currentPosition.Distance >
			targetPieceDistance {

			found = true
			break
		}

		// log.Println("CUR SIMULATION THROTTLE", currentThrottle)
		found, _ = bot.getOptimalTrottleRecursive(
			currentPosition,
			targetPieceIndex,
			targetPieceDistance,
			previousAngle,
			currentAngle,
			currentAngleVelocity,
			currentVelocity,
			currentTurnRadius,
			currentPieceAngle,
			depth+1,
		)

		if found {
			break
		}

		currentThrottle -= 0.5
	}

	if found {
		log.Println("VELOCITY", currentVelocity)
	}

	// log.Println("EXIT", found, currentThrottle)
	return found, currentThrottle
}

func (bot Noob) calcParametersAtNextTick(
	previousAngle float64,
	angle float64,
	angleVelocity float64,
	velocity float64,
	turnRadius float64,
	pieceAngle float64,
	throttle float64,
) (float64, float64, float64) {

	acceleration := bot.calcNextAcceleration(velocity, throttle)

	angleAcceleration := bot.AngleAccelEst.Eval(
		previousAngle,
		angleVelocity,
		velocity+acceleration,
		turnRadius,
		pieceAngle,
	)

	angleVelocity += angleAcceleration
	angle += angleVelocity
	velocity += acceleration

	return angle, angleVelocity, velocity
}
