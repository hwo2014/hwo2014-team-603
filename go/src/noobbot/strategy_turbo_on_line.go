package main

import ()

type StrategyTurboOnLine struct {
	Bot                       *Noob
	PerfectBoostIndex1        int
	PerfectBoostIndex2        int
	PerfectBoostIndexDetected bool
}

func (strategy StrategyTurboOnLine) Name() string {
	return "turbo_on_line"
}

func (strategy *StrategyTurboOnLine) Check() bool {
	bot := strategy.Bot

	if !strategy.PerfectBoostIndexDetected {
		strategy.PerfectBoostIndexDetected = true
		strategy.PerfectBoostIndex1, strategy.PerfectBoostIndex2 =
			bot.GetPerfectBoostPiecesIndexes()
	}

	if bot.Boost.TurboDurationTicks == 0 {
		return false
	}

	if strategy.PerfectBoostIndex1 != bot.Pos.Index &&
		strategy.PerfectBoostIndex2 != bot.Pos.Index {

		return false
	}

	opponentDistance, opponent := bot.GetClosestOpponent(bot.Pos.Lane.End, true)
	_, turnDistance := bot.GetNextTurnInfo(bot.Pos.Index, bot.Pos.Distance)
	if opponent != nil && opponentDistance < turnDistance {
		return false
	}

	return true
}

func (strategy StrategyTurboOnLine) GetActions() (float64, string, bool) {
	bot := strategy.Bot

	bot.TurboActivatedAt = bot.Tick
	bot.TurboDuration = bot.Boost.TurboDurationTicks

	bot.Boost.TurboDurationTicks = 0
	return 0, "", true
}

func (bot Noob) GetPerfectBoostPiecesIndexes() (int, int) {
	maxLength := 0.0

	maxLengthPiece := 0
	maxLengthSecondPiece := 0

	for pieceIndex, piece := range bot.Track.Pieces {
		if piece.Radius > 0 {
			continue
		}

		nextTurnIndex, distance := bot.GetNextTurnInfo(pieceIndex, 0)
		piece = bot.Track.Pieces[nextTurnIndex]

		if distance >= maxLength {
			maxLength = distance

			maxLengthSecondPiece = maxLengthPiece
			maxLengthPiece = pieceIndex
		}
	}

	return maxLengthPiece, maxLengthSecondPiece
}
