package main

import (
	"math"
)

type StrategyAccelEstimation struct {
	Bot              *Noob
	EstimationPoints []Point
}

func (s *StrategyAccelEstimation) Name() string {
	return "accel_estimation"
}

func (s *StrategyAccelEstimation) Check() bool {
	magicCoeff := 1 / 5.0
	hist := s.Bot.History[s.Bot.Tick-1]
	if math.Abs(hist.Velocity-s.Bot.Velocity) > 0 && hist.Accel != 0 {
		s.EstimationPoints = append(s.EstimationPoints, Point{
			hist.Velocity,
			s.Bot.Accel - magicCoeff*(hist.Throttle-0.5),
		})
	}

	estimateLength := 10

	localPoints := make([]Point, 0)
	dataLength := len(s.EstimationPoints)
	for i := 0; i < estimateLength; i++ {
		if i >= dataLength {
			break
		}

		idx := dataLength - estimateLength + i
		if dataLength < estimateLength {
			idx = i
		}

		localPoints = append(localPoints, s.EstimationPoints[idx])
	}

	s.Bot.AccelEst = LinearModelFit(localPoints)

	return false
}

func (s *StrategyAccelEstimation) GetActions() (float64, string, bool) {
	// 0.5 is magic
	return 0.5, "", false
}
