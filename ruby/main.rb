require 'json'
require 'socket'

max_speed = ARGV[0].to_f
turn_throttle = ARGV[1].to_f

server_host = 'senna.helloworldopen.com'
server_port = '8091'
bot_name = 'eerie'
bot_key = 'KBgsCbFn/MdE+g'

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class NoobBot
  def initialize(server_host, server_port, bot_name, bot_key, options = {})
    @options = options
    @track_pieces = nil
    @prev_data = nil
    @prev_car_position = nil
    @prev_speed = nil

    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end

  def _get_previous_passed_path()
    prev_position = @prev_car_position['piecePosition']
    prev_piece = @track_pieces[ prev_position['pieceIndex'] ]

    if prev_piece['length']
      return prev_piece['length'] - prev_position['inPieceDistance']
    else
      return @prev_speed # fuck them
      # return prev_piece['radius'] *
      #   ( prev_piece['angle'] * Math::PI / 180  ) -
      #   prev_position['inPieceDistance']
    end
  end

  def _get_circle_length( piece, distance )
  end

  def _get_speed( msgData )
    return 0 if !@prev_car_position

    if msgData['piecePosition']['pieceIndex'] ==
      @prev_car_position['piecePosition']['pieceIndex']

      return msgData['piecePosition']['inPieceDistance'] -
        @prev_car_position['piecePosition']['inPieceDistance']
    end

    # p [ _get_previous_passed_path(), msgData['piecePosition']['inPieceDistance'] ]
    return _get_previous_passed_path() +
      msgData['piecePosition']['inPieceDistance']

    @prev_speed = speed

    return speed
  end

  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      # if message['msgType'] == 'gameInit'
      #   @track_pieces
      msgType = message['msgType']
      msgData = message['data']

      case msgType
        when 'gameInit'
          @track_pieces = msgData['race']['track']['pieces']
        when 'carPositions'
          msgData = msgData.first

          speed = _get_speed( msgData )

          @prev_car_position = msgData

          current_piece = @track_pieces[ msgData['piecePosition']['pieceIndex'] ]

          throttle = 0.7

          # if !current_piece['length']
          #   throttle = @options[:turn_throttle]
          # elsif speed >= @options[:max_speed] - 0.1
          #   throttle = 0.2
          # else
          #   throttle = 1
          # end

          # puts msgData

          external_message = {
            piece: msgData['piecePosition']['pieceIndex'],
            tick: message['gameTick'],
            velocity: speed,
            acceleration: speed - @prev_speed.to_f,
            angle: msgData['angle'],
            angular_speed: msgData['angle'] - ( @prev_data || {} )['angle'].to_f,
            in_piece_distance: msgData['piecePosition']['inPieceDistance'],
            throttle: throttle,
          }.merge( Hash[ current_piece.collect {|k, v| [ "piece_#{k}", v ]} ] )

          puts JSON.generate( external_message )

          @prev_data = msgData
          @prev_speed = speed

          tcp.puts throttle_message(throttle)
          # tcp.puts throttle_message(0.5)
        else
          # case msgType
          #   when 'join'
          #     puts 'Joined'
          #   when 'gameStart'
          #     puts 'Race started'
          #   when 'crash'
          #     puts 'Someone crashed'
          #   when 'gameEnd'
          #     puts 'Race ended'
          #   when 'error'
          #     puts "ERROR: #{msgData}"
          # end
          # puts "Got #{msgType}"
          tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end
end

NoobBot.new(server_host, server_port, bot_name, bot_key, max_speed: max_speed,
  turn_throttle: turn_throttle )